#### Projeto de exemplo para a apresentação "Arquitetura em camadas em python e quanto isso pode ajudar". [Clique aqui para ver!](https://pt.slideshare.net/betterdeveloper)

## Finalidade

Esse projeto visa exemplificar o uso de camadas em um projeto simples python. Nele há branchs que mostram como implementar um projeto simples sem camadas, apenas estruturado e estruturado em camadas.

Para ver cada versão, acesse os branchs:

- [no-layers](https://gitlab.com/betterdeveloper-examples/quotes-box-layers/tree/no-layers) : Versão mais simples do projeto com apenas um app.py com todo o sistema.
- [structured](https://gitlab.com/betterdeveloper-examples/quotes-box-layers/tree/structured) : Versão com fonte estruturado em vários arquivos .py, mas ainda não aplica arquitetura em camadas.
- [layers](https://gitlab.com/betterdeveloper-examples/quotes-box-layers/tree/layers) : Versão aplicando arquitetura em camadas, com todas elas em um único projeto.

## O que este projeto não é

Não é foco deste projeto aplicar outras boas práticas de desenvolvimento de software, embora tenha-se buscado aplicar um conjunto mínimo de organização.
Há outros projetos com intuito de mostrar boas práticas de desenvolvimento. Acesse os repositórios da Better Developer no: [GitLab](gitlab.com/betterdeveloper-example) ou [GitHub](github.com/betterdeveloper-example)

## Da solução

O projeto implementa um sistema para CRUD de Citações (Quotes) através de um painel administrativo.
Também está implementado uma API em REST para consulta e CRUD das citações para que outras views possam ser criados usando a API.

## Linguagem e Tecnologias Utilizadas 

O projeto está implementado na linguagem Python na versão 3.5.
Foram utilizados os frameworks e biliotecas abaixo:
- [SQLAlchemy](http://www.sqlalchemy.org/)
- [Alembic](https://pypi.python.org/pypi/alembic)
- [Flask](http://flask.pocoo.org/)
- [Flask-Cors](https://flask-cors.readthedocs.io/en/latest/)
- [Flask-SqlAlchemy](http://flask.pocoo.org/docs/0.11/patterns/sqlalchemy/#)
- [Flask-Restless](https://flask-restless.readthedocs.io/en/stable/)
- [Flask-Admin](https://flask-admin.readthedocs.io/en/latest/)

O projeto usar Make para processo de builds do fonte. Abaixo principais targets.
- test: executa os testes.
- run: para rodar o projeto localmente.