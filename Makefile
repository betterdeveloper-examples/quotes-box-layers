.PHONY: all test clean

PYTHONPATH=.
FLASK_APP=migrate.py

all: clean test

db-autogenerate:
	flask db revision --autogenerate

db-migrate:
	flask db migrate

db-upgrade:
	flask db upgrade

db-downgrade:
	flask db downgrade

clean:
	rm -rf reports .cache
	py3clean .

test:
	pip install -r requirements-dev.txt
	pytest

code-convention:
	pip install -r requirements-dev.txt
	flake8
	pycodestyle

run:
	gunicorn -w 2 -k sync --threads 40 --preload --worker-connections 750 --timeout 31 --keep-alive 1 --backlog 2048 --log-syslog --log-syslog-prefix GUNICORN --log-level INFO wsgi:app
