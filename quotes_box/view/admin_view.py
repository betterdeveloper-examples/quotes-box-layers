from flask_admin import Admin
from flask_admin.contrib.sqla.view import ModelView

from quotes_box.domain.model import Quote, Category


class QuotesBoxAdminView():

    def __init__(self, app, session):
        self.view = Admin(app, name='Quotes Box', template_mode='bootstrap3')
        self._create_view(session)

    def _create_view(self, session):
        self.view.add_view(ModelView(Quote, session))
        self.view.add_view(CategoryView(Category, session))


class CategoryView(ModelView):
    form_excluded_columns = ['quotes']
