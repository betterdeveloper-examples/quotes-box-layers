from flask_restless.manager import APIManager

from quotes_box.domain.model import Quote, Category


class QuotesBoxApi():

    def __init__(self, app, session):

        self.api = APIManager(app, session=session)
        self._create_api()

    def _create_api(self):
        self.api.create_api(Quote, methods=['GET', 'POST', 'PUT', 'DELETE'])
        self.api.create_api(Category, methods=['GET', 'POST', 'PUT', 'DELETE'], include_columns=['id', 'name'])
