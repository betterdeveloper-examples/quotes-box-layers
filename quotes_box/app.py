from flask import Flask

from quotes_box.controller.rest.api import QuotesBoxApi
from quotes_box.domain.model import make_session
from quotes_box.view.admin_view import QuotesBoxAdminView


def create_app(config_name):

    app = Flask(__name__)
    app.config.from_object(config_name)

    session = make_session(app.config['SQLALCHEMY_DATABASE_URI'])

    #with app.app_context():
    #    db.init_app(app)
    #    db.create_all()



    with app.app_context():
        QuotesBoxApi(app, session)

    QuotesBoxAdminView(app, session)

    return app
