from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.sql.sqltypes import Integer, String, DateTime

Base = declarative_base()


def make_session(database_url):
    engine = create_engine(database_url, convert_unicode=True)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    _session = scoped_session(Session)

    Base.metadata.bind = engine
    Base.metadata.create_all()

    return _session;

class Category(Base):

    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)

    def __str__(self):
        return self.name
    

class Quote(Base):

    __tablename__ = 'quote'
    id = Column(Integer, primary_key=True)
    phrase = Column(String(1000), nullable=False)
    author = Column(String(100))
    created_time = Column(DateTime)

    category_id = Column(Integer, ForeignKey('category.id'))
    category = relationship('Category', backref=backref('quotes', lazy='dynamic'))

    def __str__(self):
        return '"{0}". ({1})'.format(self.phrase, self.author)
